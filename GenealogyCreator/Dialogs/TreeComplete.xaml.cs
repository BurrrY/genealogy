﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GenealogyCreator.Dialogs
{
    /// <summary>
    /// Interaction logic for TreeComplete.xaml
    /// </summary>
    public partial class TreeComplete : Window
    {
        public TreeComplete()
        {
            InitializeComponent();
        }

        private void PanelArea_PreviewKeyDown(object sender, KeyEventArgs e)
        {

        }

        private void PanelArea_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void PanelArea_MouseWheel(object sender, MouseWheelEventArgs e)
        {

        }

        private void PanelArea_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void PanelArea_PreviewMouseMove(object sender, MouseEventArgs e)
        {

        }

        private void PanelArea_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void zoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }

        private void zoomReset_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Grid_MouseWheel(object sender, MouseWheelEventArgs e)
        {

        }
    }
}
