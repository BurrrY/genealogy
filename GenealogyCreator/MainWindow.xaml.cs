﻿//#define CanvasDragDebug
//#define TestPanels
using ColorHelper;
using GedNet;
using GenealogyCreator.Controls;
using GenealogyCreator.src;
using Microsoft.Win32;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using SDColor = System.Drawing.Color;
using SWMColor = System.Windows.Media.Color;


namespace ColorHelper
{
    public static class ColorExt
    {
        public static SWMColor ToSWMColor(this SDColor color) => SWMColor.FromArgb(color.A, color.R, color.G, color.B);
        public static SDColor ToSDColor(this SWMColor color) => SDColor.FromArgb(color.A, color.R, color.G, color.B);
    }
}

//TODO: Use Settings-Bindings in GUI
//TODO: Use Logging-Framework


namespace GenealogyCreator
{

    public partial class MainWindow : Fluent.RibbonWindow
    {
        public enum OrderStatus { None, New, Processing, Shipped, Received };
        private Dictionary<Guid, Person> PersonList;
        private List<Family> FamilyList;
        private List<PersonPanel> PanelList;
        private Dictionary<Person, PersonPanel> panelDict;

        private Person selectedPerson;
        private List<Line> DrawnLines;
        private Dictionary<Guid, Line> LineDict;

        private Timer autosaveTimer;
        private DBBackupHandler BackupHandler;
        private Logger logger = LogManager.GetCurrentClassLogger();
        
        private double zoomFactor = 1;


        GenealogyContext db;
        public MainWindow()
        {
            logger.Warn("start");
            InitializeComponent();
            logger.Trace("Starting GenealogyCreator v" + Properties.Settings.Default.app_version.ToString());
            FamilyList = new List<Family>();
            PanelList = new List<PersonPanel>();
            PersonList = new Dictionary<Guid, Person>();
            panelDict = new Dictionary<Person, PersonPanel>();
            DrawnLines = new List<Line>();
            LineDict = new Dictionary<Guid, Line>();
            selectedPanels = new Dictionary<PersonPanel, Thickness>();

            if(Properties.Settings.Default.db_connection_string == "DEFAULT")
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                path = Path.Combine(path, "GenealogyCreator");
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                Properties.Settings.Default.db_connection_string = "Data Source=" + Path.Combine(path, "Database.gdb") + ";";
            }

            try
            {
                db = new GenealogyContext(Properties.Settings.Default.db_connection_string);
            }
            catch (Exception e)
            {
                logger.Warn(e.Message);
                throw e;
            }

            if (!db.Database.Exists())
            {
               
                try
                {
                    db.Database.Create();
                }
                catch (Exception e)
                {

                    logger.Warn(e.Message);
                    throw e;
                }
                logger.Warn("Database not found. Creating Database: " + Properties.Settings.Default.app_version.ToString());
            }



            autosaveTimer = new Timer();
            autosaveTimer.Interval = 1000*60*3;
            autosaveTimer.Elapsed += AutosaveTimer_Elapsed;
            autosaveTimer.Start();

            BackupHandler = new DBBackupHandler(Properties.Settings.Default.db_backup_interval);
            

            perGPNameEdit.Visibility = Visibility.Collapsed;
            

#if CanvasDragDebug
            SolidColorBrush brsh = new SolidColorBrush(Colors.Green);
            p1 = new System.Windows.Shapes.Rectangle();
            p2 = new System.Windows.Shapes.Rectangle();

            p1.Width = p2.Width = 5;
            p1.Height = p2.Height = 5;
            p1.Fill = p2.Fill = brsh;

            PanelArea.Children.Add(p1);
            PanelArea.Children.Add(p2);
#endif


            #region LoadSettings
            rbn_PDF_BaseFontSize.Value = Properties.Settings.Default.PDF_FontSize;
            rbn_PDF_PageWidth.Value = Properties.Settings.Default.PDF_PageWidth;

            rbn_DistHorizontal.Value = Properties.Settings.Default.autoArrangeDistanceHorizontal;
            rbn_DistVertical.Value = Properties.Settings.Default.autoArrangeDistanceVertical;

            rbn_CGChildrenSelected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.ChildLineSelectedColor);
            rbn_CGChildrenUnselected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.ChildLineUnselectedColor);

            rbn_CGSpousesSelected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.SpouseLineSelectedColor);
            rbn_CGSpousesUnselected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.SpouseLineUnselectedColor);

            rbn_CGPanelBorderSelected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.PanelBorderSelectedColor);
            rbn_CGPanelBorderUnselected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.PanelBorderUnselectedColor);



            rbn_CGMaleUnselected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.PanelBackgroudMale_Unselected);
            rbn_CGMaleSelected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.PanelBackgroudMale_Selected);

            rbn_CGFemaleUnselected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.PanelBackgroudFemale_Unselected);
            rbn_CGFemaleSelected.SelectedColor = ColorExt.ToSWMColor(Properties.Settings.Default.PanelBackgroudFemale_Selected);

            LineTypeGallery.SelectedIndex = Properties.Settings.Default.LineType;

            zoomFactor = Properties.Settings.Default.zoomFactor;
            setZoom(zoomFactor);
            #endregion


            //if (dbNew.Database.Exists() && MessageBox.Show("Delete old DB?", "?", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            //{
            //    dbNew.Database.Delete();
            //    MessageBox.Show("DB deleted");
            //}


            /*
            if(!System.IO.File.Exists("gc.db"))
            {
                if(MessageBox.Show("Database not Found!\nImport existing database?\nIf not, empty DB will be created.", "Database not found!", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    OpenFileDialog d = new OpenFileDialog();
                    if(d.ShowDialog().Value)
                    {
                        string selectedFile = d.FileName;
                        MessageBox.Show(selectedFile);
                        System.IO.File.Copy(selectedFile, "gc.db");
                    }
                }
            }
            */

            // Initialize NHibernate
            // factory = CreateNewFactory();
            // dbSession = factory.OpenSession();

            #region OLDSTUFF

            //THIS IS FOR FIRST INIT, COPY DATA FROM OLD DB
            /*
            if (MessageBox.Show("Import old DB?", "?", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {

                SQLite db = SQLite.getInstance("genealogy.db");
                Dictionary<int, Person> personList = new Dictionary<int, Person>();

                // Fetch PersonData
                DataView dv = db.exec("SELECT firstName, lastName, birth, death, placeOfBirth, infoText, p_mother, p_spouse, posX, posY, ID, isDead, isMale FROM person;");
                foreach (DataRowView row in dv)
                {
                    Person p = new Person()
                    {
                        FirstName = row["firstName"].ToString(),
                        BirthName = row["lastName"].ToString(),
                        Birth = Convert.ToDateTime(row["birth"].ToString()),
                        Death = Convert.ToDateTime(row["death"].ToString()),
                        Place = row["placeOfBirth"].ToString(),
                        InfoText = row["infoText"].ToString(),
                        PanelPosition = new Thickness(Convert.ToInt32(row["posX"].ToString()), Convert.ToInt32(row["posY"].ToString()), 0, 0),
                        isMale = row["isMale"].ToString() == "1" ? true : false,
                        isDead = row["isDead"].ToString() == "1" ? true : false
                    };

                    personList.Add(Convert.ToInt32(row["ID"].ToString()), p);

                    try
                    {

                        dbNew.Persons.Add(p);
                        dbNew.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                        throw;
                    }
                }


                //Fetch Spouses & Parents
                dv = db.exec("SELECT firstName, lastName, birth, death, placeOfBirth, infoText, p_mother, p_spouse, posX, posY, ID, isDead, isMale FROM person;");
                foreach (DataRowView row in dv)
                {
                    Person p;

                    personList.TryGetValue(Convert.ToInt32(row["ID"].ToString()), out p);


                    if (row["p_mother"].ToString() != null && row["p_mother"].ToString().Length > 0)
                    {
                        Person mother;

                        if (personList.TryGetValue(Convert.ToInt32(row["p_mother"].ToString()), out mother))
                        {
                            p.Parent = mother;
                        }
                    }


                    if (row["p_spouse"].ToString() != null && row["p_spouse"].ToString().Length > 0)
                    {
                        Person mother;

                        if (personList.TryGetValue(Convert.ToInt32(row["p_spouse"].ToString()), out mother))
                        {
                            p.Spouse = mother;
                        }
                    }
                }


                dbNew.SaveChanges();



                dv = db.exec("SELECT * FROM files;");

                foreach (DataRowView row in dv)
                {
                    int fileID = Convert.ToInt32(row["ID"].ToString());
                    int personID = Convert.ToInt32(row["p_ID"].ToString());
                    string filename = row["fileName"].ToString();

                    Document d = new Document() { Filename = filename };
                    if (d.Persons == null)
                        d.Persons = new List<Person>();


                    Person p;

                    if (personList.TryGetValue(personID, out p))
                    {
                        if (p.Documents == null)
                            p.Documents = new List<Document>();

                        p.Documents.Add(d);
                        d.Persons.Add(p);
                    }

                    dbNew.Documents.Add(d);

                }


                dbNew.SaveChanges();
            }
            */
            #endregion
            //dbSession.Flush();

            // Note that we do not use the table name specified
            // in the mapping, but the class name, which is a nice
            // abstraction that comes with NHibernate

            // List all the entries' names



#if TestPanels

            List<Person> testData = new List<Person>();
            Person a = new Person();
            a.FirstName = "Person A";
            a.PanelPosition = new Thickness(100, 100, 0, 0);
            testData.Add(a);

            Person b = new Person();
            b.FirstName = "Person B";
            b.PanelPosition = new Thickness(100, 600, 0, 0);
            testData.Add(b);

            Person c = new Person();
            c.FirstName = "Person C";
            c.PanelPosition = new Thickness(600, 300, 0, 0);
            testData.Add(c);

            Person d = new Person();
            d.FirstName = "Person D";
            d.PanelPosition = new Thickness(600, 400, 0, 0);
            testData.Add(d);
            
            List<Family> testFamily = new List<Family>();
            Family f = new Family();
            testFamily.Add(f);

            f.Children.Add(c);
            f.Children.Add(d);

            f.Spouses.Add(a);
            f.Spouses.Add(b);

            a.Spouses.Add(f);
            b.Spouses.Add(f);
            //c.isChildOf = f;
            //d.isChildOf = f;

            addPersonsToGUI(testData);
            drawFamilyLines(testFamily);
#else
            loadData();
#endif

            foreach (var f in FamilyList)
            {
                if(f.Spouses.Count > 2)
                {
                    Person a = f.Spouses.ElementAt(0);

                    for (int i = f.Spouses.Count - 1; i >= 0; i--)
                    {
                        Person p = f.Spouses.ElementAt(i);
                        if (f.Children.Contains(p))
                        {
                            p.Spouses.Remove(f);
                            f.Spouses.Remove(p);
                        }
                    }
                }
            }

            db.SaveChanges();

        }
        

        private void AutosaveTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            db.SaveChanges();
        }


        private void loadData()
        {
            var tmpPersons = db.People.Include(X => X.Events).ToList<Person>();
            FamilyList = db.Families.Include(X => X.Spouses).ToList<Family>();

            addPersonsToGUI(tmpPersons);
            drawFamilyLines(FamilyList);
        }



        #region Zooming

        private void setZoom(double zF)
        {
            if (zoomSettings == null || PanelArea == null)
                return;

            zoomFactor = zF;
            zoomSlider.Value = zoomFactor;

            var scaler = PanelArea.LayoutTransform as ScaleTransform;
            zoomFactor = Math.Round(zoomSlider.Value, 4);
            zoomSettings.Content = "Zoom: " + Math.Round(zF * 100) + "%";
            
            double scrollPosY_rel = (100 / PanelAreaScrollViewer.ScrollableHeight) * PanelAreaScrollViewer.VerticalOffset;
            double scrollPosX_rel = (100 / PanelAreaScrollViewer.ScrollableWidth) * PanelAreaScrollViewer.HorizontalOffset;

            if (scaler == null)
            {
                PanelArea.LayoutTransform = new ScaleTransform(1, 1);
            }
            else
            {
                scaler.ScaleX = zoomFactor;
                scaler.ScaleY = zoomFactor;
            }

            PanelAreaScrollViewer.UpdateLayout();
            try
            {
                if (PanelAreaScrollViewer.ScrollableHeight > 0 && !Double.IsNaN(scrollPosY_rel))
                {
                    double newScrollPosY = scrollPosY_rel / (100 / PanelAreaScrollViewer.ScrollableHeight);
                    PanelAreaScrollViewer.ScrollToVerticalOffset(newScrollPosY);
                }

                if (PanelAreaScrollViewer.ScrollableWidth > 0 && !Double.IsNaN(scrollPosX_rel))
                {
                    double newScrollPosX = scrollPosX_rel / (100 / PanelAreaScrollViewer.ScrollableWidth);
                    PanelAreaScrollViewer.ScrollToHorizontalOffset(newScrollPosX);
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
            Properties.Settings.Default.zoomFactor = zoomFactor;
            Properties.Settings.Default.Save();
        }


        private void zoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            setZoom(zoomSlider.Value);
        }

        private void zoomReset_Click(object sender, RoutedEventArgs e)
        {
            setZoom(1);
        }


        private void Grid_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                e.Handled = true;
                double d = e.Delta;
                zoomFactor = zoomFactor + (d / 1600.0);
                setZoom(zoomFactor);
            }
            else
            {
                e.Handled = false;
            }
        }

#endregion



        private void addPersonsToGUI(List<Person> personsList )
        {
            //Create Panels for each Person
            foreach (Person p in personsList)
            {
                Controls.PersonPanel panel = new Controls.PersonPanel(p);
                
                PersonList.Add(p.ID, p);

                panel.PreviewMouseLeftButtonDown += Panel_PreviewMouseLeftButtonDown;
                panel.MouseMove += Panel_MouseMove;
                panel.MouseLeftButtonUp += Panel_MouseLeftButtonUp;
                panel.PreviewKeyDown += PanelArea_PreviewKeyDown;
                panelDict.Add(p, panel);
            }


            //Draw Panels here, so they are all on top
            foreach (var pPair in panelDict)
                try
                {
                    PanelArea.Children.Add(pPair.Value);
                    Canvas.SetZIndex(pPair.Value, 100);
                }
                catch(Exception e)
                {
                    MessageBox.Show(e.Message);
                }
        }

        private void drawFamilyLines(List<Family> familyList)
        {
            foreach (var f in familyList)
            {
                foreach (var parent in f.Spouses)
                {
                    foreach (var child in f.Children)
                    {
                        drawChildLine(parent, child);
                    }

                    foreach (var spouse in f.Spouses)
                    {
                        if (parent != spouse)
                            drawSpouseLine(parent, spouse);
                    }
                }
            }


        }


        #region PanelDragnDrop

        private Point dragPanelStart;
        private Thickness startMargin;
        private Dictionary<PersonPanel, Thickness> selectedPanels;

        private void Panel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            PersonPanel pp = sender as PersonPanel;

            if (Keyboard.Modifiers != ModifierKeys.Control)
            {
                foreach (var pps in selectedPanels)
                    pps.Key.unselect();

                selectedPanels.Clear();

                selectedPanels.Add(pp, pp.Margin);
                dragPanelStart = e.GetPosition(null);
                pp.select();
                startMargin = pp.Margin;

                if((bool)rbn_cb_ShowFamilyLine.IsChecked)
                {
                    Person starter = pp.getPerson();
                    highlightChildren(starter);
                    highlightParents(starter);

                    foreach (Family f in FamilyList)
                    {
                        f.isHighlightet = false;
                    }

                }

            }
        }

        private void highlightFamilyline(Person starter)
        {
            foreach (Family f in starter.Spouses)
            {
                if (!f.isHighlightet)
                {
                    //highlightChildren(f, starter);
                    f.isHighlightet = true;
                }
            }

        }

        private void highlightParents(Person origin)
        {
            if(origin.isChildOf != null)
            {
                foreach (Person p in origin.isChildOf.Spouses)
                {
                    if (p != origin)
                    {
                        panelDict[p].select(Line.LineTarget.Parent);
                        if (!selectedPanels.ContainsKey(panelDict[p]))
                            selectedPanels.Add(panelDict[p], panelDict[p].Margin);
                        highlightParents(p);
                    }
                }
            }


            
        }

        private void highlightChildren(Person origin)
        {
            foreach (Family f in origin.Spouses)
            {
                if (!f.isHighlightet)
                {
                    foreach (Person p in f.Children)
                    {
                        if (p != origin)
                        {
                            panelDict[p].select(Line.LineTarget.Child);
                            if (!selectedPanels.ContainsKey(panelDict[p]))
                                selectedPanels.Add(panelDict[p], panelDict[p].Margin);
                            highlightChildren(p);
                        }
                    }
                }
            }
        }

        private void Panel_MouseMove(object sender, MouseEventArgs e)
        {
            // Get the current mouse position
            Point mousePos = e.GetPosition(null);
            Vector diff = dragPanelStart - mousePos;

            if (
                e.LeftButton == MouseButtonState.Pressed
                && (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance
                || Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance)
                )
            {
                
                foreach (var pps in selectedPanels)
                    pps.Key.Offset(diff, zoomFactor, pps.Value);
            }
        }
        
        private void Panel_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            PersonPanel pp = sender as PersonPanel;
            dragPanelStart = e.GetPosition(null);

            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (!pp.Selected)
                {
                    selectedPanels.Add(pp, pp.Margin);
                    startMargin = pp.Margin;
                    pp.select();
                }
                else
                {
                    selectedPanels.Remove(pp);
                    pp.unselect();
                }
            }
            else
            {
                //foreach (var pps in selectedPanels)
                //    pps.unselect();

                //selectedPanels.Clear();

                //selectedPanels.Add(pp);
                //dragPanelStart = e.GetPosition(null);
                //pp.select();
            }

            loadPersonsData(pp);

            e.Handled = true;
        }
        
#endregion
            
#region  MultiSelect
        private Point MultiSelectStart;
        private Point MultiSelectStop;

        private void PanelArea_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            PanelArea.CaptureMouse();

            foreach (var pp in selectedPanels)
                pp.Key.unselect();
            selectedPanels.Clear();


            MultiSelectStart = e.GetPosition(PanelArea);

#if CanvasDragDebug
            Point pTMP = e.GetPosition(null);
            p1.Margin = new Thickness(pTMP.X, pTMP.Y, 0, 0);
#endif

            // Initial placement of the drag selection box.         
            Canvas.SetLeft(selectionBox, MultiSelectStart.X);
            Canvas.SetTop(selectionBox, MultiSelectStart.Y);
            selectionBox.Width = 0;
            selectionBox.Height = 0;

            // Make the drag selection box visible.
            selectionBox.Visibility = Visibility.Visible;
        }

        private void PanelArea_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (MultiSelectStart.X == 0 && MultiSelectStart.Y == 0)
                return;

            PanelArea.ReleaseMouseCapture();
            selectionBox.Visibility = Visibility.Collapsed;
            MultiSelectStop = e.GetPosition(PanelArea);

#if CanvasDragDebug
            Point pTMP = e.GetPosition(null);
            pTMP.Offset(PanelArea.Margin.Left, PanelArea.Margin.Top);
            p2.Margin = new Thickness(pTMP.X, pTMP.Y, 0, 0);
#endif

            Point topLeft = new Point();
            Point bottomRight = new Point();

            topLeft.X = MultiSelectStart.X < MultiSelectStop.X ? MultiSelectStart.X : MultiSelectStop.X;
            topLeft.Y = MultiSelectStart.Y < MultiSelectStop.Y ? MultiSelectStart.Y : MultiSelectStop.Y;

            bottomRight.X = MultiSelectStart.X > MultiSelectStop.X ? MultiSelectStart.X : MultiSelectStop.X;
            bottomRight.Y = MultiSelectStart.Y > MultiSelectStop.Y ? MultiSelectStart.Y : MultiSelectStop.Y;


            foreach (var pp in panelDict)
            {
                Point p = new Point();
                p.X = pp.Value.Margin.Left;
                p.Y = pp.Value.Margin.Top;

                if( (p.X >= topLeft.X && p.X <= bottomRight.X) && (p.Y >= topLeft.Y && p.Y <= bottomRight.Y) )
                {
                    pp.Value.select();

                    if(!selectedPanels.ContainsKey(pp.Value))
                        selectedPanels.Add(pp.Value, pp.Value.Margin);
                }
            }

            MultiSelectStop.X = MultiSelectStop.Y = 0;
            MultiSelectStart.X = MultiSelectStart.Y = 0;

        }

#endregion

#region DragArea

        private Point areaDragStart;

        private void PanelArea_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            areaDragStart = e.GetPosition(PanelArea);
            e.Handled = true;
        }


        private void PanelArea_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.RightButton == MouseButtonState.Pressed) //Scroll with Right Mouse
            {
                Point mousePos = e.GetPosition(null);
                Vector diff = areaDragStart - mousePos;

                if ((Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                 Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
                {
                    PanelAreaScrollViewer.ScrollToHorizontalOffset(PanelAreaScrollViewer.HorizontalOffset - (diff.X / 2));
                    PanelAreaScrollViewer.ScrollToVerticalOffset(PanelAreaScrollViewer.VerticalOffset - (diff.Y / 2));
                }
            }
            else if (e.LeftButton == MouseButtonState.Pressed)
            {
                Point mousePos = e.GetPosition(PanelArea);

                if (MultiSelectStart.X < mousePos.X)
                {
                    Canvas.SetLeft(selectionBox, MultiSelectStart.X);
                    selectionBox.Width = mousePos.X - MultiSelectStart.X;
                }
                else
                {
                    Canvas.SetLeft(selectionBox, mousePos.X);
                    selectionBox.Width = MultiSelectStart.X - mousePos.X;
                }

                if (MultiSelectStart.Y < mousePos.Y)
                {
                    Canvas.SetTop(selectionBox, MultiSelectStart.Y);
                    selectionBox.Height = mousePos.Y - MultiSelectStart.Y;
                }
                else
                {
                    Canvas.SetTop(selectionBox, mousePos.Y);
                    selectionBox.Height = MultiSelectStart.Y - mousePos.Y;
                }
            }
        }



#endregion
        

        private void loadPersonsData(PersonPanel pp)
        {
            loadPersonsData(pp.getPerson());
        }

        private void loadPersonsData(Person p)
        {
            if (p == null)
                return;

            selectedPerson = p;

            //Load into Compact View
            loadCompactView(p);

            //
            loadPersonView(p);
        }

        private void loadPersonView(Person p)
        {
            perEventGrid.ItemsSource = null;
            perEventGrid.ItemsSource = p.Events;

            personDetailGrid.DataContext = p;
            
            perParentList.Items.Clear();
            perChildrenList.Items.Clear();
            perSpouseList.Items.Clear();

            List<Person> Parents = p.getParents().ToList<Person>();
            foreach (var prnt in Parents)
            {
                ExtendedPersonPanel ep = new ExtendedPersonPanel(prnt);
                ep.MouseDoubleClick += Ep_MouseDoubleClick;
                perParentList.Items.Add(ep);
            }

            List<Person> Children = p.getChildren().ToList<Person>();
            foreach (var prnt in Children)
            {
                ExtendedPersonPanel ep = new ExtendedPersonPanel(prnt);
                ep.MouseDoubleClick += Ep_MouseDoubleClick;
                perChildrenList.Items.Add(ep);
            }

            List<Person> Spouses = p.getSpouses().ToList<Person>();
            foreach (var prnt in Spouses)
            {
                ExtendedPersonPanel ep = new ExtendedPersonPanel(prnt);
                ep.MouseDoubleClick += Ep_MouseDoubleClick;
                perSpouseList.Items.Add(ep);
            }
        }

        private void Ep_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ExtendedPersonPanel ep = sender as ExtendedPersonPanel;
            loadPersonsData(ep.itsPerson);
        }

        private void loadCompactView(Person p)
        {
            epPerson.Load(p);

            cmpctChildrensList.Items.Clear();

            foreach (var fam in p.Spouses)
                foreach (var c in fam.Children)
                    cmpctChildrensList.Items.Add(c);

            loadParentsPanel(p, epParents);

            epHusbandParents.Clear();
            epWifeParents.Clear();
            epHusbandHusbandParents.Clear();
            epHusbandWifeParents.Clear();
            epWifeHusbandParents.Clear();
            epWifeWifeParents.Clear();
            
            loadParentsPanel(epParents.HusbandPanel.itsPerson, epHusbandParents);
            loadParentsPanel(epParents.WifePanel.itsPerson, epWifeParents);

            
            loadParentsPanel(epHusbandParents.HusbandPanel.itsPerson, epHusbandHusbandParents);
            loadParentsPanel(epHusbandParents.WifePanel.itsPerson, epHusbandWifeParents);

            
            loadParentsPanel(epWifeParents.HusbandPanel.itsPerson, epWifeHusbandParents);
            loadParentsPanel(epWifeParents.WifePanel.itsPerson, epWifeWifeParents);


            //if (!epParents.HusbandPanel.IsEmpty)
            //    loadParentsPanel(epParents.HusbandPanel.itsPerson, epHusbandParents);
            //if (!epParents.WifePanel.IsEmpty)
            //    loadParentsPanel(epParents.WifePanel.itsPerson, epWifeParents);

            
            //if (!epHusbandParents.HusbandPanel.IsEmpty)
            //    loadParentsPanel(epHusbandParents.HusbandPanel.itsPerson, epHusbandHusbandParents);
            //if (!epHusbandParents.WifePanel.IsEmpty)
            //    loadParentsPanel(epHusbandParents.WifePanel.itsPerson, epHusbandWifeParents);


            //if (!epWifeParents.HusbandPanel.IsEmpty)
            //    loadParentsPanel(epWifeParents.HusbandPanel.itsPerson, epWifeHusbandParents);
            //if (!epWifeParents.WifePanel.IsEmpty)
            //    loadParentsPanel(epWifeParents.WifePanel.itsPerson, epWifeWifeParents);

        }

        private void loadParentsPanel(Person p, SpousePanel epParents)
        {
            epParents.Clear();
            epParents.refPerson = p;

            if (p == null || p.isChildOf == null)
                return;

            if (p.isChildOf.Spouses.Count <= 0)
                return;


            Family f = p.isChildOf;

            Person A = f.Spouses.ElementAt(0);
            
            epParents.FamilyLink.Content = f.IDf.ToString().Substring(0, 8).ToUpper();

            if (A.isMale)
                epParents.HusbandPanel.Load(A);
            else
                epParents.WifePanel.Load(A);

            if(f.Spouses.Count >= 2)
            {
                Person B = f.Spouses.ElementAt(1);

                if (B.isMale)
                {
                    if(!epParents.HusbandPanel.IsEmpty)
                    {
                        MessageBox.Show("HusbandPanel already filled!");
                    }
                    else
                        epParents.HusbandPanel.Load(B);
                }
                else
                {
                    if (!epParents.WifePanel.IsEmpty)
                    {
                        MessageBox.Show("WifePanel already filled!");
                    }
                    else
                        epParents.WifePanel.Load(B);

                }

            }

        }
        
        private void btn_ResetPerson_Click(object sender, RoutedEventArgs e)
        {
            //loadPersonsData();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.Save();
            db.SaveChanges();
            
        }

        private void lb_Files_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox lb = sender as ListBox;

            if (lb.SelectedIndex < 0)
                return;




        }
        
        private void rbnAddPerson_Click(object sender, RoutedEventArgs e)
        {
           

            Person p = new Person();

            if(PersonList.ContainsKey(p.ID)) {
                MessageBox.Show("A new Person already exists! Please edit and save the last new person first.");
                return;
            }

            Controls.PersonPanel panel = new Controls.PersonPanel(p);
            PersonList.Add(p.ID, p);

            panel.PreviewMouseLeftButtonDown += Panel_PreviewMouseLeftButtonDown;
            panel.MouseMove += Panel_MouseMove;
            panel.MouseLeftButtonUp += Panel_MouseLeftButtonUp;
            panelDict.Add(p, panel);
            PanelArea.Children.Add(panel);

        }

        private void rbn_ExportDB_Click(object sender, RoutedEventArgs e)
        {

        }
        
        private void rbn_ImportDB_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, Person> personDictTMP = new Dictionary<string, Person>();
            List<Person> personListTMP = new List<Person>();
            OpenFileDialog fb = new OpenFileDialog();
            fb.Filter = "GedCom Documents|*.ged";

            bool a = (bool)fb.ShowDialog();
            if (a == true)
            {
                Gedcom gc = new Gedcom();
                gc.fromFile(fb.FileName);
                foreach (INDI i in gc.Individuals.Values)
                {
                    Person p = new Person(i);

                    db.People.Add(p);
                    personListTMP.Add(p);
                    personDictTMP.Add(p.gedcomID, p);
                }


                foreach (FAM i in gc.Families.Values)
                { 
                    Family f = new Family();

                    foreach (INDI s in i.Spouses)
                    {
                        Person p = personDictTMP[s.gedcomID];
                        p.Spouses.Add(f);
                        f.Spouses.Add(p);
                    }

                    foreach (INDI s in i.Children)
                    {
                        Person p = personDictTMP[s.gedcomID];
                        //p.isChildOf =f;
                        p.isChildOfID = f.IDf;
                        f.Children.Add(p);

                    }

                    foreach (var s in i.Events)
                        f.Events.Add(new EVENT(s));


                    FamilyList.Add(f);
                    db.Families.Add(f);
                }
            }


            addPersonsToGUI(personListTMP);
            drawFamilyLines(FamilyList);


            db.SaveChanges();
        }
        
        private void drawSpouseLine(Person parent, Person spouse)
        {
            drawLine(parent, spouse, true);
        }

        private void drawChildLine(Person parent, Person child)
        {
            drawLine(parent, child, false);
        }

        private void drawLine(Person persA, Person persB, bool isSpouseLine)
        {
            bool tryGetA;
            bool tryGetB;

            PersonPanel pPanelA;
            PersonPanel pPanelB;

            tryGetA = panelDict.TryGetValue(persA, out pPanelA);
            tryGetB = panelDict.TryGetValue(persB, out pPanelB);

            if (tryGetA && tryGetB)
            {
                if(!pPanelA.LineExists(persB.ID))
                {
                    Line l = new Line(ref pPanelA, ref pPanelB, isSpouseLine, persA, persB, (Line.LineType)Properties.Settings.Default.LineType);

                    DrawnLines.Add(l);
                    foreach (UIElement uiLine in l.getLines())
                    {
                        PanelArea.Children.Add(uiLine);
                        Canvas.SetZIndex(uiLine, 10);
                    }

                    if (isSpouseLine)
                    {
                        pPanelA.addLine(ref l, Line.LineTarget.Spouse);
                        pPanelB.addLine(ref l, Line.LineTarget.Spouse);
                    }
                    else
                    {
                        pPanelA.addLine(ref l, Line.LineTarget.Child);
                        pPanelB.addLine(ref l, Line.LineTarget.Parent);
                    }
                }
            }
        }

        private void rbn_CreatePDF_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "PDF documents (.pdf)|*.pdf"; // Filter files by extension
            sfd.FileName = "Genealogy"; // Default file name
            sfd.DefaultExt = ".pdf"; // Default file extension

            // Show save file dialog box
            Nullable<bool> result = sfd.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string filename = sfd.FileName;
                PDFCreator p = new PDFCreator(filename);
                p.createPDF();
            }

        }
        

        private void rbn_DistVertical_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Properties.Settings.Default.autoArrangeDistanceVertical = (int)rbn_DistVertical.Value;
            Properties.Settings.Default.Save();

        }

        private void rbn_DistHorizontal_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Properties.Settings.Default.autoArrangeDistanceHorizontal = (int)rbn_DistHorizontal.Value;
            Properties.Settings.Default.Save();
        }

        private void rbn_CGPanelBorderSelected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.PanelBorderSelectedColor = ColorExt.ToSDColor((SWMColor)rbn_CGPanelBorderSelected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }

        private void rbn_CGPanelBorderUnselected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.PanelBorderUnselectedColor = ColorExt.ToSDColor((SWMColor)rbn_CGPanelBorderUnselected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }

        private void rbn_CGChildrenSelected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.ChildLineSelectedColor = ColorExt.ToSDColor((SWMColor)rbn_CGChildrenSelected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }

        private void rbn_CGChildrenUnselected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.ChildLineUnselectedColor = ColorExt.ToSDColor((SWMColor)rbn_CGChildrenUnselected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }

        private void rbn_CGSpousesSelected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.SpouseLineSelectedColor = ColorExt.ToSDColor((SWMColor)rbn_CGSpousesSelected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }

        private void rbn_CGMaleUnselected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.PanelBackgroudMale_Unselected = ColorExt.ToSDColor((SWMColor)rbn_CGMaleUnselected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }

        private void rbn_CGFemaleUnselected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.PanelBackgroudFemale_Unselected = ColorExt.ToSDColor((SWMColor)rbn_CGFemaleUnselected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }

        private void rbn_CGMaleSelected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.PanelBackgroudMale_Selected = ColorExt.ToSDColor((SWMColor)rbn_CGMaleSelected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }

        private void rbn_CGFemaleSelected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.PanelBackgroudFemale_Selected = ColorExt.ToSDColor((SWMColor)rbn_CGFemaleSelected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }





        private void rbn_CGSpousesUnselected_SelectedColorChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.SpouseLineUnselectedColor = ColorExt.ToSDColor((SWMColor)rbn_CGSpousesUnselected.SelectedColor);
            Properties.Settings.Default.Save();
            foreach (var item in panelDict.Values)
                item.updateColor();
        }

        private void rbn_PDF_PageWidth_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Properties.Settings.Default.PDF_PageWidth = (int)rbn_PDF_PageWidth.Value;
            Properties.Settings.Default.Save();
        }

        private void rbn_PDF_BaseFontSize_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Properties.Settings.Default.PDF_FontSize = (int)rbn_PDF_BaseFontSize.Value;
            Properties.Settings.Default.Save();
        }

        private void lineGal_Straight_Click(object sender, RoutedEventArgs e)
        {
            saveLineType(Line.LineType.Straight);
        }

        private void lineGal_RectA_Click(object sender, RoutedEventArgs e)
        {
            saveLineType(Line.LineType.Rect_A);
        }

        private void lineGal_RectB_Click(object sender, RoutedEventArgs e)
        {
            saveLineType(Line.LineType.Rect_B);
        }

        private void lineGal_AngleA_Click(object sender, RoutedEventArgs e)
        {
            saveLineType(Line.LineType.Angular_A);
        }

        private void saveLineType(Line.LineType t)
        {
            Properties.Settings.Default.LineType = (int)t;
            Properties.Settings.Default.Save();
            LineTypeGallery.SelectedIndex = Properties.Settings.Default.LineType;
            foreach (PersonPanel p in panelDict.Values)
                p.UpdateLine();

            //Delete all Lines
            for(int i = PanelArea.Children.Count-1; i>=0; --i)
            {
                UIElement item = PanelArea.Children[i];
                if (item.GetType() == typeof(System.Windows.Shapes.Line))
                {
                    PanelArea.Children.Remove(item);
                }
            }


            foreach (PersonPanel p in panelDict.Values)
                foreach (Line l in p.Lines.Keys)
                    foreach (UIElement uiLine in l.getLines())
                    {
                        try
                        {
                            if (!PanelArea.Children.Contains(uiLine))
                            {
                                PanelArea.Children.Add(uiLine);
                                Canvas.SetZIndex(uiLine, 10);
                            }

                        }
                        catch (Exception e)
                        {
                            MessageBox.Show(e.Message);
                        }
                    }
        }


        #region Arrangement

        private Dictionary<int, int> RowInCol;
        private int generationWidth;
        private int generationHeight;

        private void rbn_OrderAutomatic_Click(object sender, RoutedEventArgs e)
        {
            if(MessageBox.Show("Alle Personen automatisch anordnen?", "Bitte bestätigen!", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                return;

            generationWidth = 180 + Properties.Settings.Default.autoArrangeDistanceHorizontal;
            generationHeight = 60 + Properties.Settings.Default.autoArrangeDistanceVertical;

            RowInCol = new Dictionary<int, int>();
            int generationColumn = 15;
            
            foreach (Person pTmp in PersonList.Values)
                pTmp.isArranged = false;
            

            Person p = PersonList.Values.First();
            p.isArranged = true;
            PersonPanel panel = panelDict[p];

            if (!RowInCol.ContainsKey(generationColumn))
                RowInCol.Add(generationColumn, 0);

            int RowPos = RowInCol[generationColumn];
            RowPos += generationHeight;

            panel.updatePosition(new Thickness(generationColumn * generationWidth, RowPos, 0, 0));

            RowInCol[generationColumn] = RowPos;

            //Recursive call
            arrangePersonsFamily(p, generationColumn);
        }

        private void arrangePersonsFamily(Person p, int generationColumn)
        {
            Family parentsFamily = p.isChildOf;
            if (parentsFamily != null)
            {
                arrangePanelsToColumn(generationColumn, parentsFamily.Children.ToList<Person>(), p);
                generationColumn -= 1;
                arrangePanelsToColumn(generationColumn, parentsFamily.Spouses.ToList<Person>(), p);

                generationColumn += 1;
            }


            foreach (Family f in p.Spouses)
            {
                arrangePanelsToColumn(generationColumn, f.Spouses.ToList<Person>(), p);
                arrangePanelsToColumn(generationColumn+1, f.Children.ToList<Person>(), p);
            }
        }
        
        private void arrangePanelsToColumn(int generationColumn, List<Person> PersonList, Person pExclude = null)
        {
            foreach (Person p in PersonList)
            {
                if ((pExclude != null && p == pExclude) || p.isArranged)
                    continue;

                PersonPanel panel = panelDict[p];


                if (!RowInCol.ContainsKey(generationColumn))
                    RowInCol.Add(generationColumn, 0);

                int RowPos = RowInCol[generationColumn];
                RowPos += generationHeight;
                panel.updatePosition(new Thickness((generationColumn) * generationWidth, RowPos, 0, 0));

                RowInCol[generationColumn] = RowPos;
                p.isArranged = true;
                

                arrangePersonsFamily(p, generationColumn);
            }
        }
                
        private void rbn_spreadVertical_Click(object sender, RoutedEventArgs e)
        {
            spreadPanelsVertical();
        }

        private void rbn_spreadHorizontal_Click(object sender, RoutedEventArgs e)
        {
            spreadPanelsHorizontal();
        }

        private void rbn_alignTop_Click(object sender, RoutedEventArgs e)
        {
            alignPanelsTop();
        }

        private void rbn_alignBottom_Click(object sender, RoutedEventArgs e)
        {
            alignPanelsBottom();
        }

        private void rbn_alignLeft_Click(object sender, RoutedEventArgs e)
        {
            alignPanelsLeft();
        }

        private void rbn_alignRight_Click(object sender, RoutedEventArgs e)
        {
            alignPanelsRight();
        }



        private void alignPanelsRight()
        {
            double maxX = -1;

            foreach (PersonPanel pp in selectedPanels.Keys)
                if (pp.Margin.Left > maxX)
                    maxX = pp.Margin.Left;

            for (int i = 0; i < selectedPanels.Count; ++i)
            {
                PersonPanel pp = selectedPanels.ElementAt(i).Key;
                pp.updatePosition(new Thickness(maxX, pp.Margin.Top, 0, 0));
                selectedPanels[pp] = pp.Margin;
            }
        }

        private void alignPanelsLeft()
        {
            double minX = Double.MaxValue;

            foreach (PersonPanel pp in selectedPanels.Keys)
                if (pp.Margin.Left < minX)
                    minX = pp.Margin.Left;

            for (int i = 0; i < selectedPanels.Count; ++i)
            {
                PersonPanel pp = selectedPanels.ElementAt(i).Key;
                pp.updatePosition(new Thickness(minX, pp.Margin.Top, 0, 0));
                selectedPanels[pp] = pp.Margin;
            }
        }

        private void alignPanelsTop()
        {
            double minY = Double.MaxValue;

            foreach (PersonPanel pp in selectedPanels.Keys)
                if (pp.Margin.Top < minY)
                    minY = pp.Margin.Top;

            for (int i = 0; i < selectedPanels.Count; ++i)
            {
                PersonPanel pp = selectedPanels.ElementAt(i).Key;
                pp.updatePosition(new Thickness(pp.Margin.Left, minY, 0, 0));
                selectedPanels[pp] = pp.Margin;
            }
        }

        private void alignPanelsBottom()
        {
            double maxY = -1;

            foreach (PersonPanel pp in selectedPanels.Keys)
                if (pp.Margin.Top > maxY)
                    maxY = pp.Margin.Top;

            for (int i = 0; i < selectedPanels.Count; ++i)
            {
                PersonPanel pp = selectedPanels.ElementAt(i).Key;
                pp.updatePosition(new Thickness(pp.Margin.Left, maxY, 0, 0));
                selectedPanels[pp] = pp.Margin;
            }
        }

        private void spreadPanelsVertical()
        {
            double minY = Double.MaxValue, maxY = 0;
            double k;
            int cnt = 0;
            Dictionary<double, PersonPanel> tmpDict = new Dictionary<double, PersonPanel>();

            foreach (PersonPanel pp in selectedPanels.Keys)
            {
                if (pp.Margin.Top < minY)
                    minY = pp.Margin.Top;

                if (pp.Margin.Top > maxY)
                    maxY = pp.Margin.Top;

                k = pp.Margin.Top;
                while (tmpDict.ContainsKey(k))
                    k++;

                tmpDict.Add(k, pp);
            }

            IOrderedEnumerable<KeyValuePair<double, PersonPanel>> orderedList = tmpDict.OrderBy(key => key.Key);

            double distanceTotal = maxY - minY;

            double distanceBetween = distanceTotal / (selectedPanels.Count - 1);

            foreach (var ptemp in orderedList)
            {
                PersonPanel pp = ptemp.Value;
                pp.updatePosition(new Thickness(pp.Margin.Left, minY + distanceBetween * cnt, 0, 0));
                selectedPanels[pp] = pp.Margin;
                ++cnt;
            }
        }

        private void spreadPanelsHorizontal()
        {
            double minX = Double.MaxValue, maxX = 0;
            double k;
            int cnt = 0;
            Dictionary<double, PersonPanel> tmpDict = new Dictionary<double, PersonPanel>();

            foreach (PersonPanel pp in selectedPanels.Keys)
            {
                if (pp.Margin.Left < minX)
                    minX = pp.Margin.Left;

                if (pp.Margin.Left > maxX)
                    maxX = pp.Margin.Left;

                k = pp.Margin.Left;
                while (tmpDict.ContainsKey(k))
                    k++;

                tmpDict.Add(k, pp);
            }

            IOrderedEnumerable<KeyValuePair<double, PersonPanel>> orderedList = tmpDict.OrderBy(key => key.Key);

            double distanceTotal = maxX - minX;

            double distanceBetween = distanceTotal / (selectedPanels.Count - 1);

            foreach (var ptemp in orderedList)
            {
                PersonPanel pp = ptemp.Value;
                pp.updatePosition(new Thickness(minX + distanceBetween * cnt, pp.Margin.Top, 0, 0));
                selectedPanels[pp] = pp.Margin;
                ++cnt;
            }
        }


        #endregion

        private void PanelArea_PreviewKeyDown(object sender, KeyEventArgs e)
        {

            if(selectedPanels.Count > 0 &&   ((e.Key == Key.Down) || (e.Key == Key.Up) || (e.Key == Key.Left) || (e.Key == Key.Right)) )
            {
                int delta = 5;

                if(Keyboard.Modifiers == ModifierKeys.Control)
                    delta *= 15;

                for (int i=0;i<selectedPanels.Count;++i)
                {
                    PersonPanel pp = selectedPanels.ElementAt(i).Key;
                    if (e.Key == Key.Down)
                        pp.updatePosition(new Thickness(pp.Margin.Left, pp.Margin.Top + delta, 0, 0));
                    else if (e.Key == Key.Up)
                        pp.updatePosition(new Thickness(pp.Margin.Left, pp.Margin.Top - delta, 0, 0));
                    else if (e.Key == Key.Left)
                        pp.updatePosition(new Thickness(pp.Margin.Left - delta, pp.Margin.Top, 0, 0));
                    else if (e.Key == Key.Right)
                        pp.updatePosition(new Thickness(pp.Margin.Left + delta, pp.Margin.Top, 0, 0));

                    selectedPanels[pp] = pp.Margin;
                }

                e.Handled = true;
            }
        }

        private void rbn_File_Save_Click(object sender, RoutedEventArgs e)
        {
            db.SaveChanges();
        }

        private void rbn_File_Exit_Click(object sender, RoutedEventArgs e)
        {

        }

        private void RibbonWindow_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            TabItem currentMainTab = MainTabControl.SelectedItem as TabItem;

            if (currentMainTab.Name == "CompleteTreeTab") { 
                if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.S)
                    db.SaveChanges();
                else if (e.Key == Key.NumPad4)
                    alignPanelsLeft();
                else if (e.Key == Key.NumPad8)
                    alignPanelsTop();
                else if (e.Key == Key.NumPad6)
                    alignPanelsRight();
                else if (e.Key == Key.NumPad2)
                    alignPanelsBottom();
                else if (e.Key == Key.NumPad1)
                    spreadPanelsVertical();
                else if (e.Key == Key.NumPad9)
                    spreadPanelsHorizontal();
            }
        }


        private void epParents_PersonDoubleClick(object sender, Person e)
        {
            loadPersonsData(e);
        }

        private void cmpctChildrensList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox lb = sender as ListBox;

            if (lb.SelectedItems.Count <= 0)
                return;

            Person p = lb.SelectedItem as Person;
            loadPersonsData(p);

        }


        private void SpousePanel_OnAddPersonClick(object sender, AddPersonEventArgs e)
        {

            if (e.refPerson == null)
                return;

            if (MessageBox.Show("Soll eine neue Person angelegt werden?", "", MessageBoxButton.YesNo)== MessageBoxResult.No)
                return;

            //Sometimes, there are Multiple Families.
            if((e.relation == Family.Relationships.Daughter || 
                e.relation == Family.Relationships.Son || 
                e.relation == Family.Relationships.Children) &&
                e.refPerson.Spouses.Count > 1)
            {
                MessageBox.Show("Familie auswählen oder neue Erstellen");

            }

            Person newPerson = new Person(e.refPerson, e.relation);

            PersonList.Add(newPerson.ID, newPerson);


            if((newPerson.isChildOf != null && !FamilyList.Contains(newPerson.isChildOf)))            
                FamilyList.Add(newPerson.isChildOf);

            foreach (var fam in newPerson.Spouses)
                if (!FamilyList.Contains(fam))
                    FamilyList.Add(fam);


            loadPersonsData(newPerson);

            Controls.PersonPanel panel = new Controls.PersonPanel(newPerson);
            panel.PreviewMouseLeftButtonDown += Panel_PreviewMouseLeftButtonDown;
            panel.MouseMove += Panel_MouseMove;
            panel.MouseLeftButtonUp += Panel_MouseLeftButtonUp;
            panel.PreviewKeyDown += PanelArea_PreviewKeyDown;
            panelDict.Add(newPerson, panel);
      

            PanelArea.Children.Add(panel);
            Canvas.SetZIndex(panel, 100);
              


        }

        private void perName_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            perGPNameEdit.Visibility = Visibility.Visible;

        }

        private void perBtnSave_Click(object sender, RoutedEventArgs e)
        {
            perGPNameEdit.Visibility = Visibility.Collapsed;
        }

        private void genderString_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnPerAddPerson_Click(object sender, RoutedEventArgs e)
        {
            Family.Relationships r;
            TabItem currentMainTab = TcPerRelatives.SelectedItem as TabItem;

            switch (currentMainTab.Name)
            {
                case "TabChildren":
                    r = Family.Relationships.Children;
                    break;
                case "TabSpouses":
                    r = Family.Relationships.Spouse;
                    break;
                case "TabParents":
                    r = Family.Relationships.Parent;
                    break;
                default:
                    return;
                    break;
            }

            SpousePanel_OnAddPersonClick(this, new AddPersonEventArgs(selectedPerson,  r));
        }

        private void rbn_OpenDatabase_Click(object sender, RoutedEventArgs e)
        {
            db.SaveChanges();
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Multiselect = false;
            ofd.ShowDialog();
            ofd.Filter = "Genealogy Database (.gdb)|*.gdb"; // Filter files by extension

            if (ofd.FileName != null && ofd.FileName.Length>0)
            {
                clearData();
                string connectionString = "Data Source=" + ofd.FileName + ";";
                db = new GenealogyContext(connectionString);

                Properties.Settings.Default.db_connection_string = connectionString;
                Properties.Settings.Default.Save();

                loadData();
            }
        }

        private void clearData()
        {
            autosaveTimer.Stop();

            PanelArea.Children.Clear();


            FamilyList = new List<Family>();
            PanelList = new List<PersonPanel>();
            PersonList = new Dictionary<Guid, Person>();
            panelDict = new Dictionary<Person, PersonPanel>();
            DrawnLines = new List<Line>();
            LineDict = new Dictionary<Guid, Line>();
            selectedPanels = new Dictionary<PersonPanel, Thickness>();
            
        }

        private void rbn_NewDatabase_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.AddExtension = true;
            sfd.DefaultExt = ".gdb";
            sfd.Filter = "Genealogy Database (.gdb)|*.gdb"; // Filter files by extension


            Nullable<bool> result = sfd.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                // Open document
                string filename = sfd.FileName;
                if (File.Exists(filename))
                {
                    MessageBox.Show("File already exists!");
                    return;
                }
                else
                {
                    clearData();

                    string connectionString = "Data Source=" + sfd.FileName + ";";
                    db = new GenealogyContext(connectionString);
                    db.Database.CreateIfNotExists();

                    Properties.Settings.Default.db_connection_string = connectionString;
                    Properties.Settings.Default.Save();
                }
            
            }
        }

        private void rbn_DB_Interval_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if(BackupHandler != null)
                BackupHandler.updateInterval();
        }

        private void rbn_File_Settings_Click(object sender, RoutedEventArgs e)
        {
            Dialogs.SettingsDialog settingsDialog = new Dialogs.SettingsDialog();
            settingsDialog.ShowDialog();
        }
    }
}
