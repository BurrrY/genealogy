﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenealogyCreator.src
{
    public class SQLite
    {
        internal static SQLite _instance;

        private SQLiteConnection connection;

        public static SQLite getInstance(string path)
        {
            if (_instance == null)
                _instance = new SQLite(path);

            return _instance;
        }


        private SQLite(string Path)
        {
            connect(Path);
        }

        private void connect(string path)
        {
            connection = new SQLiteConnection();
            bool create = !System.IO.File.Exists(path);

            connection.ConnectionString = "Data Source=" + path;
            try
            {
                connection.Open();
            }
            catch (TypeInitializationException e)
            {
                throw e;
            }
            catch (SQLiteException ex)
            {
                throw ex;
            }

            if (create)
            {
                exec("CREATE TABLE \"person\" (`ID`	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,`firstName` TEXT,`lastName`	TEXT,`birth`	TEXT,`death`	TEXT,`p_father`	INTEGER,`p_mother`	INTEGER,`p_spouse`	INTEGER,`placeOfBirth`	TEXT,`posX`	INTEGER,`posY`	INTEGER,`infoText`	TEXT,`posM`	INTEGER DEFAULT '0', isDead integer, isMale integer);");
                exec("CREATE TABLE `files` ( `ID`	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, `fileName` TEXT, `p_ID`	INTEGER );");
            }
        }





        public DataView exec(string SQLComm)
        {
            DataTable data = new DataTable();

            if (connection == null)
                return null;

            try
            {
                SQLiteCommand command = new SQLiteCommand(connection);
                command.CommandText = SQLComm;
                SQLiteDataReader reader = command.ExecuteReader();
                data.Load(reader);
            }
            catch (SQLiteException crap)
            {
                throw crap;
            }

            DataView DataView = new DataView(data);
            return DataView;
        }


        public void execNonQuerySQL(string SQLComm)
        {
            if (connection == null)
                return;


            try
            {
                SQLiteCommand command = new SQLiteCommand(connection);
                command.CommandText = SQLComm;
                SQLiteDataReader reader = command.ExecuteReader();
            }
            catch (SQLiteException crap)
            {
                throw crap;
            }
        }

        public void close()
        {
            connection.Close();
        }
        
    }
}
