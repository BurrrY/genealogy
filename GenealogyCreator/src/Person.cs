﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Windows;
using GedNet;
using System.Windows.Data;

namespace GenealogyCreator.src
{
    public class Person : INDI
    {


        public string BirthName { get; set; }


        public string Place { get; set; }

        public bool isMale {
            get
            {
                return Sex == 'M';
            }
            set
            {
                if (value)
                    Sex = 'M';
                else
                    Sex = 'F';

            }
        }

        public bool isDead { get; set; }
        public string InfoText { get; set; }


        public int X { get; set; }
        public int Y { get; set; }

        #region from GEDNET
        public INDI i { get; private set; }

        public Family isChildOf { get; set; }
        public Guid? isChildOfID { get; set; }

        public virtual ICollection<Family> Spouses { get; set; }

        [NotMapped]
        public bool isArranged { get; set; }
        #endregion



        //Non-Persistent Data
        [NotMapped]
        public virtual Thickness PanelPosition
        {
            get
            {
                return new Thickness(X, Y, 0, 0);
            }
            set
            {
                X = (int)value.Left;
                Y = (int)value.Top;
            }
        }
        [NotMapped]
        protected ThreeLine MothersLine { get; set; }
        [NotMapped]
        protected Line Line { get; set; }
        [NotMapped]
        protected List<Person> Children { get; set; }
        [NotMapped]
        public virtual Controls.PersonPanel itsPanel { get; set; }
        [NotMapped]
        public virtual bool spouseLineDrawn { get; set; }
        [NotMapped]
        public virtual bool parentLineDrawn { get; set; }


        [NotMapped]
        public string GenderString { get { return (isMale ? "Männlich" : "Weiblich");  } private set { } }

        
        public Person()
        {
            Spouses = new List<Family>();
            spouseLineDrawn = false;
            parentLineDrawn = false;
            FirstName = "Vorname";
            LastName = "Nachname";

            ID = Guid.NewGuid();
        }

        public Person(INDI i)
        {
            spouseLineDrawn = false;
            parentLineDrawn = false;
            Spouses = new List<Family>();

            this.i = i;
            ID = Guid.NewGuid();

            gedcomID = i.gedcomID;
            Changed = i.Changed;
            Sex = i.Sex;
            gedcomID = i.gedcomID;
            LastName = i.LastName;
            FirstName = i.FirstName;
            Phone = i.Phone;

            Address = new ADDR(i.Address);

            foreach (EVENT e in i.Events)
                Events.Add(new EVENT(e));

            foreach (NOTE e in i.Notes)
                Notes.Add(new NOTE(e));

            foreach (SOUR_USE e in i.Sources)
                Sources.Add(new SOUR_USE(e));

            foreach (OBJE e in i.Objects)
                Objects.Add(new OBJE(e));

        }

        public Person(Person refPerson, Family.Relationships relation)
        {
            spouseLineDrawn = false;
            parentLineDrawn = false;
            Spouses = new List<Family>();
            this.LastName = "von " + refPerson.ToString();

            switch (relation)
            {
                case Family.Relationships.Mother:
                case Family.Relationships.Father:
                case Family.Relationships.Parent:
                    {
                        this.FirstName = "Elternteil";
                        Family refFam = refPerson.isChildOf;

                        if (refFam == null)
                        {
                            refFam = new Family();
                            refPerson.isChildOf = refFam;
                            refFam.Children.Add(refPerson);
                        }
                        this.PanelPosition = refPerson.PanelPosition;

                        Spouses.Add(refFam);
                        refFam.Spouses.Add(this);

                        if (relation == Family.Relationships.Father)
                            isMale = true;
                        else
                            isMale = false;
                    }
                    break;
                case Family.Relationships.Wife:
                case Family.Relationships.Husband:
                case Family.Relationships.Spouse:
                    this.FirstName = "Ehepartner";


                    if (relation == Family.Relationships.Husband)
                        isMale = true;
                    else
                        isMale = false;


                    break;
                case Family.Relationships.Daughter:
                case Family.Relationships.Son:
                case Family.Relationships.Children:
                    {
                        this.FirstName = "Kind";
                        Family refFam = null;

                        if (refPerson.Spouses.Count == 0)
                        {
                            refFam = new Family();
                            refFam.Spouses.Add(refPerson);
                            refPerson.Spouses.Add(refFam);
                        }
                        else
                        {
                            refFam = (refPerson.Spouses as List<Family>)[0];
                        }

                        isChildOf = (refFam);
                        refFam.Children.Add(this);

                        if (relation == Family.Relationships.Son)
                            isMale = true;
                        else
                            isMale = false;
                    }
                    break;
                case Family.Relationships.Sister:
                case Family.Relationships.Brother:
                case Family.Relationships.Sibling:
                    this.FirstName = "Ehepartner";


                    if (relation == Family.Relationships.Brother)
                        isMale = true;
                    else
                        isMale = false;

                    break;
                default:
                    break;
            }
        }

        public override string ToString()
        {
            return FirstName + " " + LastName;
        }

        internal ICollection<Person> getParents()
        {
            if (isChildOf != null)
                return isChildOf.Spouses;
            else
                return new List<Person>();
        }

        internal ICollection<Person> getChildren()
        {
            ICollection<Person> res = new List<Person>();

            foreach (var f in Spouses)
            {
                foreach (var c in f.Children)
                {
                    res.Add(c);
                }
            }

            return res;
        }

        internal ICollection<Person> getSpouses()
        {
            ICollection<Person> res = new List<Person>();

            foreach (var f in Spouses)
            {
                foreach (var c in f.Spouses)
                {
                    if(c != this)
                        res.Add(c);
                }
            }

            return res;
        }
    }
}
