﻿using ColorHelper;
using GenealogyCreator.Controls;
using System;
using System.Windows;
using System.Windows.Media;
using System.Collections.Generic;


using uiLine = System.Windows.Shapes.Line;
using System.Diagnostics;

namespace GenealogyCreator.src
{
    public class Line {

        public enum LineType
        {
            Straight,
            Rect_A,
            Rect_B,
            Angular_A,
            Angular_B
        }

        public Guid ID;
        public Thickness Start;
        public Thickness End;
        //public System.Windows.Shapes.Line LineElement;
        public SolidColorBrush Brush = new SolidColorBrush();
        private List<UIElement> Lines = new List<UIElement>();

        private LineType Type;
        private Guid personA;
        private Guid personB;
        private bool isSelected;
        private bool isSpouseLine;
        private double strokeWidth = 4;
        private string Name;

        public enum LineTarget
        {
            Spouse,
            Parent,
            Child,
            All
        }

        public override string ToString()
        {
            return Name;
        }

        public Line(ref PersonPanel PanelA, ref PersonPanel PanelB, bool Spouses, Person ID_A, Person ID_B, LineType lT = LineType.Angular_A)
        {
            ID = Guid.NewGuid();
            personA = ID_A.ID;
            personB = ID_B.ID;

            Name = ID_A.FirstName + "<>" + ID_B.FirstName;


            Start = PanelA.Margin;
            End = PanelB.Margin;

            isSelected = false;
            isSpouseLine = Spouses;
            Type = lT;




            initType();
            setLine();


        }
        private void setLine()
        {

            if (Type == LineType.Straight)
            {
                uiLine tmp = Lines[0] as uiLine;
                tmp.X1 = Start.Left + 60;
                tmp.Y1 = Start.Top + 30;
                tmp.X2 = End.Left + 60;
                tmp.Y2 = End.Top + 30;
                tmp.StrokeThickness = strokeWidth;
                tmp.Stroke = Brush;
            }
            else if (Type == LineType.Rect_A)
            {
                uiLine tmpA = Lines[0] as uiLine;
                uiLine tmpB = Lines[1] as uiLine;

                tmpA.X1 = Start.Left + 60;
                tmpA.X2 = End.Left + 60;
                tmpA.Y1 = Start.Top + 30;
                tmpA.Y2 = Start.Top + 30;

                tmpA.StrokeThickness = strokeWidth;
                tmpA.Stroke = Brush;


                tmpB.X1 = End.Left + 60;
                tmpB.X2 = End.Left + 60;
                tmpB.Y1 = Start.Top + 30;
                tmpB.Y2 = End.Top + 30;
                tmpB.StrokeThickness = strokeWidth;
                tmpB.Stroke = Brush;
            }
            else if (Type == LineType.Rect_B)
            {
                uiLine tmpA = Lines[0] as uiLine;
                uiLine tmpB = Lines[1] as uiLine;

                tmpA.X1 = Start.Left + 60;
                tmpA.X2 = Start.Left + 60;
                tmpA.Y1 = Start.Top + 30;
                tmpA.Y2 = End.Top + 30;

                tmpA.StrokeThickness = strokeWidth;
                tmpA.Stroke = Brush;


                tmpB.X1 = Start.Left + 60;
                tmpB.X2 = End.Left + 60;
                tmpB.Y1 = End.Top + 30;
                tmpB.Y2 = End.Top + 30;
                tmpB.StrokeThickness = strokeWidth;
                tmpB.Stroke = Brush;
            }
            else if (Type == LineType.Angular_A)
            {
                //bool useX, useY;
                //string LeftPart;
                //string TopPart;


                uiLine tmpA = Lines[0] as uiLine;
                uiLine tmpB = Lines[1] as uiLine;
                uiLine tmpC = Lines[2] as uiLine;

                double distX = Math.Abs(Start.Left - End.Left);
                double distY = Math.Abs(Start.Top - End.Top);

                Point A, B, C, D = new Point();

                double smallPart = 0;


                if (distX < distY)
                {
                    smallPart = distX * 0.5;

                    if (Start.Left < End.Left)
                        B = new Point(Start.Left + smallPart, Start.Top);
                    else
                        B = new Point(Start.Left - smallPart, Start.Top);

                    
                }
                else
                {
                    smallPart = distY * 0.5;

                    if (Start.Left < End.Left)
                        B = new Point(End.Left - smallPart, Start.Top);
                    else
                        B = new Point(End.Left + smallPart, Start.Top);

                }


                if (Start.Top > End.Top)
                    C = new Point(End.Left, Start.Top - smallPart);
                else
                    C = new Point(End.Left, Start.Top + smallPart);




                A = new Point(Start.Left, Start.Top);


                D = new Point(End.Left, End.Top);

                //Debug.Write("LeftPart" + ": " + LeftPart);
                //Debug.Write("\tTopPart" + ": " + TopPart);
                //Debug.Write("\tsmallPart" + ": " + smallPart);
                //Debug.Write("\tuseY" + ": " + useY);
                //Debug.Write("\tdistX" + ": " + distX);
                //Debug.WriteLine("\tdistY" + ": " + distY);

                A.Offset(60, 30);
                B.Offset(60, 30);
                C.Offset(60, 30);
                D.Offset(60, 30);

                setPoint(tmpA, A, 1);
                setPoint(tmpA, B, 2);

                setPoint(tmpB, B, 1);
                setPoint(tmpB, C, 2);
                
                setPoint(tmpC, C, 1);
                setPoint(tmpC, D, 2);

                
                tmpA.StrokeThickness = strokeWidth;
                tmpA.Stroke = Brush;
                tmpB.StrokeThickness = strokeWidth;
                tmpB.Stroke = Brush;
                tmpC.StrokeThickness = strokeWidth;
                tmpC.Stroke = Brush;
            }

                updateColor();
        }

        internal void UpdateLine()
        {
            Type = (Line.LineType)Properties.Settings.Default.LineType;
            initType();
            setLine();
        }

        private void initType()
        {
            Lines.Clear();
            if (Type == LineType.Straight)
            {
                uiLine LineElement = new uiLine();
                Lines.Add(LineElement);
            }
            else if (Type == LineType.Rect_A || Type == LineType.Rect_B)
            {

                uiLine LineElementA = new uiLine();
                uiLine LineElementB = new uiLine();
                Lines.Add(LineElementA);
                Lines.Add(LineElementB);
            }
            else if (Type == LineType.Angular_B || Type == LineType.Angular_A)
            {

                uiLine LineElementA = new uiLine();
                uiLine LineElementB = new uiLine();
                uiLine LineElementC = new uiLine();
                Lines.Add(LineElementC);
                Lines.Add(LineElementA);
                Lines.Add(LineElementB);
            }
        }

        private void setPoint(uiLine L, Point p, int pos)
        {
            if(pos == 1 )
            {
                L.X1 = p.X;
                L.Y1 = p.Y;
            }
            else
            {
                L.X2 = p.X;
                L.Y2 = p.Y;
            }
        }
        
        
        public static bool operator ==(Line a, Line b)
        {
            if (((a.personA == b.personA && a.personB == b.personB) ||
                    (a.personA == b.personB && a.personB == b.personA)))
                return true;
            else
                return false;


        }

        public static bool operator !=(Line a, Line b)
        {
            return !(a == b);
        }


        public bool Equals(Line b)
        {
            if (((personA == b.personA && personB == b.personB) ||
                    (personA == b.personB && personB == b.personA)))
                return true;
            else
                return false;
        }
        

        internal void updatePosition(Thickness newPos, Thickness oldPos)
        {
            if (Start == oldPos)
                Start = newPos;
            else
                End = newPos;

            setLine();
        }

        internal bool isInbetween(Guid iD1, Guid iD2)
        {
           if((iD1 == personA && iD2 == personB) || (iD2 == personA && iD1 == personB))
                return true;
            else
                return false;
        }

        internal void updateColor()
        {
            if (isSelected)
                select();
            else
                unselect();
        }

        internal void unselect()
        {
            isSelected = false;

            if (isSpouseLine)
                Brush.Color = ColorExt.ToSWMColor(Properties.Settings.Default.SpouseLineUnselectedColor);
            else
                Brush.Color = ColorExt.ToSWMColor(Properties.Settings.Default.ChildLineUnselectedColor);

            Brush.Opacity = 0.5;           
        }

        internal void select()
        {
            isSelected = true;

            if (isSpouseLine)
                Brush.Color = ColorExt.ToSWMColor(Properties.Settings.Default.SpouseLineSelectedColor);
            else
                Brush.Color = ColorExt.ToSWMColor(Properties.Settings.Default.ChildLineSelectedColor);

            Brush.Opacity = 1;
        }

        internal IEnumerable<UIElement> getLines()
        {
            return Lines;
        }
    }

    public class ThreeLine
    {
        public Thickness Start;
        public Thickness Center;
        public Thickness End;

        // Create a red Brush
        SolidColorBrush lineBrush = new SolidColorBrush()
        {
            Color = Colors.Blue
        };
        double strokeWidth = 4;


        public ThreeLine(Thickness margin1, Thickness margin2)
        {
            Start = margin1;
            Start.Left += 60;
            Start.Top += 30;

            End = margin2;
            End.Left += 60;
            End.Top += 30;

            Center = new Thickness(End.Left, Start.Top, 0, 0);
        }

        public System.Windows.Shapes.Line getLineA()
        {
            System.Windows.Shapes.Line l = new System.Windows.Shapes.Line();
            l.X1 = Start.Left;
            l.Y1 = Start.Top;
            l.X2 = Center.Left;
            l.Y2 = Center.Top;


            // Set Line's width and color
            l.StrokeThickness = strokeWidth;
            l.Stroke = lineBrush;

            return l;
        }



        public System.Windows.Shapes.Line getLineB()
        {
            System.Windows.Shapes.Line l = new System.Windows.Shapes.Line();
            l.X2 = End.Left;
            l.Y2 = End.Top;
            l.X1 = Center.Left;
            l.Y1 = Center.Top;

            
            // Set Line's width and color
            l.StrokeThickness = strokeWidth;
            l.Stroke = lineBrush;

            return l;
        }
    }
}
