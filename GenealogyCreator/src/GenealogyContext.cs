﻿using GedNet;
using SQLite.CodeFirst;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenealogyCreator.src
{
    class GenealogyContext : DbContext
    {
        public DbSet<Source> Sources { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<Family> Families { get; set; }
        //public DbSet<Object> Objects { get; set; }

        //public GenealogyContext()
        //: base("Data Source=" + Properties.Settings.Default.databaseFile) { }
        
            
        public GenealogyContext(string connectionString)
            : base(new SQLiteConnection() { ConnectionString = connectionString }, true)
        {

        }

        


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var sqliteConnectionInitializer = new SqliteDropCreateDatabaseWhenModelChanges<GenealogyContext>(modelBuilder);
            Database.SetInitializer(sqliteConnectionInitializer);

            //modelBuilder.Entity<Person>()
            //            .HasRequired(i => i.isChildOf)
            //            .WithMany()                        
            //           .HasForeignKey(i => i.isChildOfID);




            modelBuilder.Entity<Person>()
                        .HasOptional<ADDR>(s => s.Address);

            modelBuilder.Entity<Event>()
                        .HasOptional<ADDR>(s => s.Address);

            modelBuilder.Entity<Person>()
                        .HasOptional<Family>(s => s.isChildOf)
                        .WithMany(s => s.Children)
                        .HasForeignKey(s => s.isChildOfID);





            modelBuilder.Entity<Family>()
            .HasMany(i => i.Spouses)
            .WithMany(c => c.Spouses)
            .Map(cs =>
            {
                cs.MapLeftKey("FamilySRefId");
                cs.MapRightKey("PersonSRefId");
                cs.ToTable("PersonFamily");
            });

        }
    }
}
