﻿using GedNet;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenealogyCreator.src
{
    public class Family 
    {
        [Key]
        public Guid IDf { get; set; }

        public virtual ICollection<Person> Spouses { get; set; }
        public virtual ICollection<Person> Children { get; set; }
        public List<EVENT> Events { get; set; }

        [NotMapped]
        public bool isHighlightet;

        public enum Relationships
        {
            Mother,
            Father,
            Wife,
            Husband,
            Daughter,
            Son,
            Sister,
            Brother,
            Children,
            Parent,
            Spouse,
            Sibling
        }

        public override string ToString()
        {
            return IDf.ToString();
        }

        public Family(FAM i)
        {
            IDf = Guid.NewGuid();
            isHighlightet = false;
            Spouses = new List<Person>();
            Children = new List<Person>();
            Events = new List<EVENT>();

            foreach (INDI s in i.Spouses)
                Spouses.Add(s as Person);

            foreach (INDI s in i.Children)
                Children.Add(s as Person);

            foreach (EVENT s in i.Events)
                Events.Add(s);            
        }

        public Family()
        {
            IDf = Guid.NewGuid();
            Spouses = new List<Person>();
            Children = new List<Person>();
            Events = new List<EVENT>();
        }
    }
}
