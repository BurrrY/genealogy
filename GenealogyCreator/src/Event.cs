﻿using GedNet;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenealogyCreator.src
{
    public class Event : EVENT
    {

        public Event() :base(TYPES.UNKOWN)
        {
            IDevent = Guid.NewGuid();
        }

        public Event(TYPES eventType) : base(eventType)
        {
            IDevent = Guid.NewGuid();
        }
    }
}
