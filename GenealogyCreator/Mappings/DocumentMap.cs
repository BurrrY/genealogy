﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenealogyCreator.Mappings
{
    public class DocumentMap : ClassMap<src.Document>
    {

        public DocumentMap()
        {
            Id(x => x.ID);
            Map(x => x.Filename);

            HasManyToMany<src.Person>(x => x.Persons).Table("PersonDocument").Inverse();
        }
    }
}
