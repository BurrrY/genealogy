﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GenealogyCreator.src;
using GedNet;

namespace GenealogyCreator.Controls
{
    /// <summary>
    /// Interaction logic for ExtendedPersonPanel.xaml
    /// </summary>
    public partial class ExtendedPersonPanel : UserControl
    {
        public Person itsPerson { get; private set; }
        public event EventHandler OnAddPersonClick;
        public bool IsEmpty { get; internal set; }

        public ExtendedPersonPanel()
        {
            InitializeComponent();
            Clear();
        }

        public ExtendedPersonPanel(Person p)
        {
            InitializeComponent();
            Clear();
            Load(p);
        }

        internal void Clear()
        {
            IsEmpty = true;
            itsPerson = null;
            personName.Text = "";
            personInfo.Text = "";
            GenderIndicator.Background = Brushes.Gray;
            personInfo.Visibility = Visibility.Collapsed;
            personName.Visibility = Visibility.Collapsed;
            AddPersonPanel.Visibility = Visibility.Visible;
        }


        internal void Load(Person p)
        {
            itsPerson = p;
            writePersonData();
            IsEmpty = false;

            if (itsPerson.isMale)
                GenderIndicator.Background = Brushes.Blue;
            else
                GenderIndicator.Background = Brushes.Red;

            personInfo.Visibility = Visibility.Visible;
            personName.Visibility = Visibility.Visible;
            AddPersonPanel.Visibility = Visibility.Collapsed;
        }

        private void writePersonData()
        {
            personName.Text = itsPerson.FirstName + " " + itsPerson.LastName;

            StringBuilder PersonInfoTxt = new StringBuilder();

            if (itsPerson.BirthName != null && itsPerson.BirthName.Length > 0)
                PersonInfoTxt.Append("( " + itsPerson.BirthName + " )\n");


            EVENT birth = itsPerson.getEvent(GedNet.EVENT.TYPES.BIRTH);
            if (birth != null)
                PersonInfoTxt.Append("*" + birth.Date + " " + birth.Place + "\n");

            EVENT death = itsPerson.getEvent(GedNet.EVENT.TYPES.DEATH);
            if (death != null)
                PersonInfoTxt.Append(" - " + death.Date + " " + death.Place + "\n");

            personInfo.Text = PersonInfoTxt.ToString();

            if (itsPerson.isMale)
                this.Background = new SolidColorBrush(Color.FromRgb(170, 215, 250));
            else
                this.Background = new SolidColorBrush(Color.FromRgb(250, 215, 170));
        }

        private void AddPersonPanel_MouseUp(object sender, MouseButtonEventArgs e)
        {
            OnAddPersonClick?.Invoke(this, EventArgs.Empty);
        }
    }
}
