﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GenealogyCreator.Controls
{
    /// <summary>
    /// Interaction logic for AddPersonPanel.xaml
    /// </summary>
    public partial class AddPersonPanel : UserControl
    {
        public AddPersonPanel()
        {
            InitializeComponent();
        }

        private void UserControl_MouseEnter(object sender, MouseEventArgs e)
        {
            Border b = sender as Border;
            b.Background = Brushes.LightGray;
            Mouse.OverrideCursor = Cursors.Hand;
        }

        private void UserControl_MouseLeave(object sender, MouseEventArgs e)
        {
            Border b = sender as Border;
            b.Background = Brushes.White;
            Mouse.OverrideCursor = null;
        }
    }
}
