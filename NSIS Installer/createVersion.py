import xmltodict

fd = open('Files/GenealogyCreator.exe.config');
data = fd.read();
fd.close();

while(data[0] != "<"):
	data = data[1:];
	

doc = xmltodict.parse(data);
appSettings =  {};
settingArray = doc["configuration"]["applicationSettings"]["GenealogyCreator.Properties.Settings"]["setting"];
for setting in settingArray:
	appSettings[setting["@name"]] = setting["value"];

print(appSettings);

fd = open('version.nsh', 'w');
fd.write("!define VERSION " + appSettings["app_version"] + "\n");
fd.write("!define RELEASE_NAME " + appSettings["app_releaseName"] + "\n");
fd.write("!define RELEASE_DATE " + appSettings["app_releaseDate"] + "\n");